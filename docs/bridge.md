<div align="center">
  <h1>Bridge</h1>
</div>

<div align="center">
  <img src="bridge_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Bridge is a structural pattern that lets you develop abstraction and implementation hierarchies
independently of each other.**

### Real-World Analogy

_A configurable universal remote control._

Allows users (clients) to interact with the TV, stereo and smart home system (concrete implementations) using a remote
control that can have multiple configurations (abstractions).

### Participants

- :bust_in_silhouette:/:man: **Abstraction**
    - Usually an abstract class, with mostly abstract methods and only some with an implementation.
    - Defines abstract methods for:
        - `highLevelMethodA`: to be implemented by RefinedAbstraction
    - Provides implementation for:
        - `highLevelMethodB`: implemented using an Implementor
- :man: **RefinedAbstraction** (optional)
    - Can be omitted if the Abstraction has no abstract methods and there is no need to overwrite any of them.
    - Provides implementation for:
        - `highLevelMethodA`: implemented using an Implementor
- :bust_in_silhouette: **Implementor**
    - Defines an interface for:
        - `primitiveMethodA`
- :man: **Concretelmplementor**
    - Provides implementation for:
        - `primitiveMethodA`

### Collaborations

**Abstraction** forwards client requests to its **Implementor** object

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When the abstraction can have one of several implementations, or the implementation can
change at runtime.**

### Motivation

- How can we divide and organize a class that has several variants of functionality (E.g: GUI that works on various
  OSes)
- How do we develop abstractions and implementations independently of each other?
- How can we allow switching the implementation used by an abstraction at runtime?
- How can we share implementations among multiple objects?

### Known Uses

- Task Management Tool with Task Types and Notification Preferences:
    - Abstractions: Task types (e.g., to-dos, reminders, events).
    - Implementors: Notification preferences (e.g., email, in-app, SMS).
- A streaming app that supports various streaming services and different devices.
    - Abstractions: Streaming services (e.g. YouTube, Twitch)
    - Implementors: Devices (e.g. webcam, DSLR camera)
- A trading app that supports different trading bots and various exchanges:
    - Abstractions: trading bots (AverageTrader, MinMaxTrader)
    - Implementors: exchange connectors (Binance, Coinbase)
- Smart Home System with Devices and Communication Protocols:
    - Abstractions: Smart home devices (e.g., lights, thermostats, locks).
    - Implementors: Communication protocols (e.g., Zigbee, Z-Wave, Wi-Fi) for device connectivity.
- Customer Relationship Management (CRM) Software with Customer Segments and Communication Channels:
    - Abstractions: Customer segments (e.g., leads, prospects, loyal customers).
    - Implementors: Communication channels (e.g., email, sms, whatsapp).
- A user interface which supports various OSes and different user roles.
    - Abstractions: User roles (e.g. client, admin).
        - Admins see different parts of the user interface than clients can see.
    - Implementors: Oses (e.g. windows, macOS, android)
- Frontend Data Access Layer:
    - An Abstraction defining an Entity Service that will handle logic related to the entities within your system.
    - An Implementation defining an API Interface that allows you to interact with any potential backend system or API.

### Categorization

Purpose:  **Structural**  
Scope:    **Object**   
Mechanisms: **Composition**, **Polymorphism**, "Inheritance"

Structural patterns are concerned with how classes and objects are composed to form larger structures.
Structural object patterns describe ways to compose objects to realize new functionality.

### Aspects that can vary

- Implementation of an object.

### Solution to causes of redesign

- Dependence on hardware and software platform.
    - harder to port to other platforms.
    - may even be difficult to keep it up to date on its native platform.
- Dependence on object representations or implementations.
    - Clients that know how an object is represented, stored, located, or implemented might need to be changed when the
      object changes.
- Tight coupling.
    - Hard to understand: a lot of context needs to be known to understand a part of the system.
    - Hard to change: changing one class necessitates changing many other classes.
    - Hard to reuse in isolation: because classes depend on each other.
- Extending functionality by subclassing.
    - Hard to understand: comprehending a subclass requires in-depth knowledge of all parent classes.
    - Hard to change: a modification to a parent might break multiple children classes.
    - Hard to reuse partially: all class members are inherited, even when not applicable, which can lead to a class
      explosion.

### Consequences

| Advantages                                                                                                                                                                           | Disadvantages                                                                           |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|
| :heavy_check_mark: **Implementation not permanently bound to interface.** <br>The implementation of an abstraction can be configured at run-time.                                    | :x: **Increases complexity** <br> Applying the pattern adds more classes to the system. |
| :heavy_check_mark: **Eliminates compile-time dependence on implementation.** <br>Changing an implementation class doesn't require recompiling the Abstraction class and its clients. |                                                                                         |
| :heavy_check_mark: **Encourages layering.** <br>The high-level part of system only has to know about Abstraction and Implementor.                                                    |                                                                                         |
| :heavy_check_mark: **Improved extensibility.** <br>You can extend the Abstraction and Implementor hierarchies independently                                                          |                                                                                         |

### Relations with Other Patterns

_Distinction from other patterns:_

- At first sight, the Bridge pattern looks a lot like the Adapter pattern in that a class is used to convert one kind of
  interface to another.
    - The Adapter pattern is geared toward making unrelated classes work together. It is usually applied to systems
      after they're designed.
    - Bridge, on the other hand, is used up-front in a design to let abstractions and implementations vary
      independently.
- Allowing to switch implementation at runtime is the main reason why the Bridge is confused with the Strategy pattern.
    - While the Strategy pattern is meant for behavior, the Bridge pattern is meant for structure.

_Combination with other patterns:_

- An Abstract Factory can be used to create and configure a particular Bridge.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **An abstract Abstraction class with methods that redirect translated calls to a stored
Implementor object. And, RefinedAbstraction classes that extend the Abstraction.**

### Structure

```mermaid
classDiagram
    class Abstraction {
        <<abstract>>
        - implementor: Implementor
        + highLevelMethodA()*
        + highLevelMethodB()
    }

    class RefinedAbstractionA {
        - implementor: Implementor
        + highLevelMethodA()
    }

    class RefinedAbstractionB {
        - implementor: Implementor
        + highLevelMethodA()
    }

    class Implementor {
        <<interface>>
        + primitiveMethodA()*
    }

    class ConcreteImplementorA {
        + primitiveMethodA()
    }

    class ConcreteImplementorB {
        + primitiveMethodA()
    }

    RefinedAbstractionA --|> Abstraction: extends
    RefinedAbstractionB --|> Abstraction: extends
    Implementor <|.. ConcreteImplementorA: implements
    Implementor <|.. ConcreteImplementorB: implements
    Abstraction *-- Implementor: composes
```

### Variations

- **Degenerate Bridge**:  When there's only one implementation, the abstract Implementor class isn't necessary
    - :heavy_check_mark: still useful when a change in the implementation of a class must not affect its existing
      clients

### Implementation

In the example we apply the bridge pattern to a task management app where you can select notification preferences per
task type (to-dos, reminders, events).

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Bridge](https://refactoring.guru/design-patterns/bridge)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [ArjanCodes: Let's Take The Bridge Pattern To The Next Level](https://youtu.be/mM2-FPm1EhI?si=-NYqxixaVsbbu0ZL)
- [ArjanCodes: Two UNDERRATED Design Patterns 💡 Write BETTER PYTHON CODE Part 6](https://youtu.be/t0mCrXHsLbI?si=kbFtVbmafALyTS1R)
- [Ravi Bhatt: bridge pattern](https://medium.com/coding-becomes-easy/bridge-pattern-48150246e552)
- [The Bridge Pattern - Design Patterns meet the Frontend](https://dev.to/coly010/the-bridge-pattern-design-patterns-meet-the-frontend-46fc)

<br>
<br>
